@extends('app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        @include('feed.message-box')
      </div>
    </div><!-- /row -->

    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        <div class="feed">
          @foreach ($chimes as $chime)
            @include('feed.chime', $chime)
          @endforeach
        </div>

      </div>
    </div><!-- /row -->
  </div>
@endsection