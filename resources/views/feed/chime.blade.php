<div class="row">
  <div class="col-md-12">
    <div class="chime {{ $chime->mine ? 'mine' : '' }}">
      <header>
        <small class="pull-right">{{  date('j M Y H:i', strtotime($chime->created_at)) }}</small>
        <strong><span>@</span>{{ $chime->user->username }}</strong> ({{ $chime->user->firstname }} {{ $chime->user->surname }})
      </header>
      <p>{!! nl2br(e($chime->message)) !!}</p>
    </div>
  </div>
</div>