<div class="message-box">
  {!! Form::open() !!}
    {!! Form::textarea('message', null, ['id' => 'message', 'class' => 'form-control', 'maxlength' => '140', 'rows' => 3, 'placeholder' => 'Your message']) !!}

    <div class="put">
      {!! Form::submit('Post', ['class' => 'btn btn-lg btn-default pull-right']) !!}
      <div class="chars-left pull-right"><strong>140</strong> characters left</div>
    </div>
  {!! Form::close() !!}
</div>

@if ($errors->has())
  <div class="alert alert-danger">
    {{ $errors->first('message') }}
  </div>
@endif