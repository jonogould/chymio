<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <center>{{ date('Y') }} &copy; jonogould.com</center>
      </div>
    </div>
  </div>
</footer>