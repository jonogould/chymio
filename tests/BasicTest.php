<?php

class BasicTest extends TestCase {

  /**
   * Ensure that the home page gets redirected to the login page
   *
   * @return void
   */
  public function testRedirectedToAuth()
  {
    $this->call('GET', '/');

    $this->assertRedirectedTo('auth/login');
  }

}
