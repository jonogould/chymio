/**
 * main.js
 *
 * @author @jonogould
 */
(function($) {
  if ($('.message-box').length) {
    $('#message').on('keyup', function() {
      var left = 140 - $(this).val().length;
      $('.chars-left strong').text(left);
    });
  }
})(jQuery);