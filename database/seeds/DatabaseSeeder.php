<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Chime;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
    $this->command->info('User table seeded!');

    $this->call('ChimeTableSeeder');
    $this->command->info('Chimes table seeded!');
	}

}
