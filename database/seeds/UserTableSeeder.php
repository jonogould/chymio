<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Chime;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::table('chimes')->delete();
    DB::table('users')->delete();

    User::create([
      'username' => 'jonogould',
      'firstname' => 'Jono',
      'surname' => 'Gould',
      'email' => 'jono@gould.co.za',
      'password' => Hash::make( 'password' )
    ]);

    User::create([
      'username' => 'bob001',
      'firstname' => 'Bob',
      'surname' => 'Dylan',
      'email' => 'bobd@gmail.com',
      'password' => Hash::make( 'password' )
    ]);
	}

}
