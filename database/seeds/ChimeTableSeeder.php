<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Chime;

class ChimeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		DB::table('chimes')->delete();

    Chime::create([
      'user_id' => 2,
      'message' => 'A hero is someone who understands the responsibility that comes with his freedom.'
    ]);

    Chime::create([
      'user_id' => 2,
      'message' => 'Take care of all your memories. For you cannot relive them.'
    ]);

    Chime::create([
      'user_id' => 2,
      'message' => 'No one is free, even the birds are chained to the sky.'
    ]);

    Chime::create([
      'user_id' => 2,
      'message' => 'There is nothing so stable as change.'
    ]);
	}

}
