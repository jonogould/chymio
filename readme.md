# Chymio 2015

[![jonogould](https://img.shields.io/badge/jonogould--blue.svg)](http://jonogould.com)

Pronounced like: chime-eo.

A simple "Twitter-like" platform to share quick posts with friends.

Runs on top of [Laravel 5](http://laravel.com). Read the docs [here](http://laravel.com/docs/5.0).

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Requirements

- nginx
- MySQL
- PHP >= 5.4
- Mcrypt PHP Extension
- OpenSSL PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- [PHP Composer](https://getcomposer.org/)

## Installation

1. Clone the bitbucket repo from [https://bitbucket.org/jonogould/chymio](https://bitbucket.org/jonogould/chymio) into a folder on your local environment, and navigate into it using terminal.

2. Add the following to your nginx site configuration file (I created a new server block with a port of 8555):

```
  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }
```

3. Create an empty ```chymio``` database on your local machine.

4. Copy ```.env.example``` to ```.env``` and change the details to correspond to your database.

5. Run ```php composer.phar update``` (or whereever your composer file is stored, I have my aliased to ```composer```) and wait for the downloads to finish

## Migrating and Seeding the database

From the root chymio folder (using your command line), run:

1. ```php artisan migrate```

To create the db tables. If your db settings above are correct it should give you happy messages.

2. ```php artisan db:seed```

To seed the tables with dummy data

## Happy browsing!

Open your browser and everything should be up and running :)