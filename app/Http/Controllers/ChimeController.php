<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Chime;

class ChimeController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $chimes = Chime::orderBy('id', 'desc')->simplePaginate(30);

    foreach ($chimes as $chime) {
      if ($chime->user_id === Auth::user()->id) {
        $chime->mine = true;
      } else {
        $chime->mine = false;
      }
    }

    return view('feed.index', ['chimes' => $chimes]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
    $this->validate($request, [
        'message' => 'required|max:140'
    ]);

    $data = $request->all();

		Chime::create([
      'message' => $data['message'],
      'user_id' => Auth::user()->id
    ]);

    return new RedirectResponse(url('/'));
	}

}
